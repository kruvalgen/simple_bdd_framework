package ru.lanit.at.steps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.ru.И;
import io.qameta.allure.Allure;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import ru.lanit.at.api.ApiRequest;
import ru.lanit.at.api.models.RequestModel;
import ru.lanit.at.api.testcontext.ContextHolder;
import ru.lanit.at.utils.CompareUtil;
import ru.lanit.at.utils.DataGenerator;
import ru.lanit.at.utils.Sleep;
import ru.lanit.at.utils.VariableUtil;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.apache.commons.lang3.StringUtils.split;
import static ru.lanit.at.api.testcontext.ContextHolder.getValue;
import static ru.lanit.at.api.testcontext.ContextHolder.replaceVarsIfPresent;
import static ru.lanit.at.utils.JsonUtil.getFieldFromJson;

public class ApiSteps {


    private static final Logger LOG = LoggerFactory.getLogger(ApiSteps.class);
    private ApiRequest apiRequest;

    @И("создать запрос")
    public void createRequest(RequestModel requestModel) {
        apiRequest = new ApiRequest(requestModel);
    }

    @И("добавить header")
    public void addHeaders(Map<String, String> headers) {
        apiRequest.setHeaders(headers);
    }

    @И("добавить query параметры")
    public void addQuery(Map<String, String> query) {
        apiRequest.setQuery(query);
    }

    @И("добавить query параметры из {string}")
    public void addQueryFromUrl(String urlKey) {
        Map<String, String> queryParam = new HashMap<>();
        String url = ContextHolder.getValue(urlKey);
        url = url.split("\\?")[1];
        for (String query : url.split("&")) {
            String[] var = query.split("=");
            queryParam.put(var[0], var[1]);
        }
        apiRequest.setQuery(queryParam);
    }

    @И("отправить запрос")
    public void send() {
        apiRequest.sendRequest();
    }

    @И("статус код {int}")
    public void expectStatusCode(int code) {
        int actualStatusCode = apiRequest
                .getResponse()
                .statusCode();
        Assert.assertEquals(actualStatusCode, code);
    }

    @И("извлечь данные")
    public void extractVariables(Map<String, String> vars) {
        String responseBody;
        if (apiRequest.getResponse().contentType().startsWith("text/html")) {
            responseBody = apiRequest.getResponse().htmlPath().getString("html.body");
            responseBody = responseBody.replaceAll("<[\\/]?[\\w]*>", "");
            responseBody = responseBody.replaceAll("\\\\\"", "\\\"");
            responseBody = responseBody.replaceAll("\\\"\\[", "[");
            responseBody = responseBody.replaceAll("\\]\\\"", "]");
        } else {
            responseBody = apiRequest.getResponse().body().asPrettyString();
        }

        String finalResponseBody = responseBody;
        vars.forEach((k, jsonPath) -> {
            String extractedValue = VariableUtil.extractBrackets(getFieldFromJson(finalResponseBody, jsonPath));
            ContextHolder.put(k, extractedValue);
            Allure.addAttachment(k, "application/json", extractedValue, ".txt");
            LOG.info("Извлечены данные: {}={}", k, extractedValue);
        });
    }


    @И("сгенерировать переменные")
    public void generateVariables(Map<String, String> table) {
        table.forEach((k, v) -> {
            String value = DataGenerator.generateValueByMask(replaceVarsIfPresent(v));
            ContextHolder.put(k, value);
            Allure.addAttachment(k, "application/json", k + ": " + value, ".txt");
            LOG.info("Сгенерирована переменная: {}={}", k, value);
        });
    }

    @И("создать контекстные переменные")
    public void createContextVariables(Map<String, String> table) {
        table.forEach((k, v) -> {
            ContextHolder.put(k, v);
            LOG.info("Сохранена переменная: {}={}", k, v);
        });
    }


    @И("сравнить значения")
    public void compareVars(DataTable table) {
        table.asLists().forEach(it -> {
            String expect = replaceVarsIfPresent(it.get(0));
            String actual = replaceVarsIfPresent(it.get(2));
            boolean compareResult = CompareUtil.compare(expect, actual, it.get(1));
            Assert.assertTrue(compareResult, String.format("Ожидаемое: '%s'\nФактическое: '%s'\nОператор сравнения: '%s'\n", expect, actual, it.get(1)));
            Allure.addAttachment(expect, "application/json", expect + it.get(1) + actual, ".txt");
            LOG.info("Сравнение значений: {} {} {}", expect, it.get(1), actual);
        });
    }

    @И("сравнить и заполнить если истина")
    public void compareAndFill(DataTable table) {
        table.asLists().forEach(it -> {
            String key = replaceVarsIfPresent(it.get(0));
            String operator = it.get(1);
            String value = replaceVarsIfPresent(it.get(2));
            String valueGen = DataGenerator.generateValueByMask(it.get(3));
            boolean compareResult = CompareUtil.compare(ContextHolder.getValue(key), value, operator);
            if (compareResult) {
                ContextHolder.put(key, valueGen);
                LOG.info("Для пустого поля {} сгенерировано значение {} ", key, valueGen);
            }
        });
    }


    @И("подождать {int} сек")
    public void waitSeconds(int timeout) {
        Sleep.pauseSec(timeout);
    }
}
