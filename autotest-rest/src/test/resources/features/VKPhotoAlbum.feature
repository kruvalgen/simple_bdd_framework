#language:ru
#@test

Функционал: VK API
  -	Создать приватный альбом
  -	Добавить фотографию в альбом
  -	Сделать фотографию обложкой альбома
  -	Прокомментировать фотографию
  -	Добавить отметку на фотографию
  -	Создать публичный альбом
  -	Перенести туда фотографию
  -	Удалить первый альбом

  Сценарий: работа с фотоальбомом
#  Создать приватный альбом

    * сгенерировать переменные
      | title | EEEEEEEE |

    * создать запрос
      | method | path                |
      | GET    | /photos.createAlbum |

    * добавить query параметры

      | title        | ${title} |
      | privacy_view | nobody   |

    * отправить запрос
    * статус код 200
#  Добавить фотографию в альбом
    * извлечь данные
      | id | $..id |

    * создать запрос
      | method | path                    |
      | GET    | /photos.getUploadServer |

    * добавить query параметры
      | album_id | ${id} |

    * отправить запрос

    * извлечь данные
      | upload_url | $..upload_url |

    * создать запрос
      | method | url           | body   |
      | POST   | ${upload_url} | 13.jpg |

    * добавить header
      | Content-Type | multipart/form-data            |
      | accept       | text/html charset=windows-1251 |


    * добавить query параметры из "upload_url"

    * отправить запрос

    * извлечь данные
      | server      | $..server      |
      | photos_list | $..photos_list |
      | hash        | $.hash         |
      | aid         | $.aid          |

    * создать запрос
      | method | path         |
      | GET    | /photos.save |

    * добавить query параметры
      | album_id    | ${id}          |
      | server      | ${server}      |
      | photos_list | ${photos_list} |
      | hash        | ${hash}        |
      | aid         | ${aid}         |

    * отправить запрос
    * статус код 200

#  Сделать фотографию обложкой альбома

    * извлечь данные
      | photo_id | $..id       |
      | owner_id | $..owner_id |
      | album_id | $..album_id |

    * создать запрос
      | method | path              |
      | GET    | /photos.makeCover |

    * добавить query параметры
      | photo_id | ${photo_id} |
      | owner_id | ${owner_id} |
      | album_id | ${album_id} |

    * отправить запрос
    * статус код 200

#  Прокомментировать фотографию
    * сгенерировать переменные
      | message | EEEERRRR |
    * создать запрос
      | method | path                  |
      | GET    | /photos.createComment |

    * добавить query параметры
      | photo_id | ${photo_id} |
      | owner_id | ${owner_id} |
      | message  | ${message}  |

    * отправить запрос
    * статус код 200

#  Добавить отметку на фотографию
    * создать запрос
      | method | path           |
      | GET    | /photos.putTag |

    * добавить query параметры
      | photo_id | ${photo_id} |
      | owner_id | ${owner_id} |
      | user_id  | 273521727   |
      | x        | 15          |
      | y        | 12          |
      | x2       | 25          |
      | y2       | 30          |

    * отправить запрос
    * статус код 200

#  Создать публичный альбом
    * сгенерировать переменные
      | title | ERRRRREEEE |

    * создать запрос
      | method | path                |
      | GET    | /photos.createAlbum |

    * добавить query параметры
      | title        | ${title} |
      | privacy_view | any      |

    * отправить запрос
    * статус код 200

#  Перенести туда фотографию

    * извлечь данные
      | id2    | $..id    |
      | title2 | $..title |

    * создать запрос
      | method | path         |
      | GET    | /photos.move |

    * добавить query параметры
      | photo_id        | ${photo_id} |
      | owner_id        | ${owner_id} |
      | target_album_id | ${id2}      |

    * отправить запрос
    * статус код 200

#  Удалить первый альбом
    * создать запрос
      | method | path                |
      | GET    | /photos.deleteAlbum |

    * добавить query параметры
      | owner_id | ${owner_id} |
      | album_id | ${id}       |

    * отправить запрос
    * статус код 200