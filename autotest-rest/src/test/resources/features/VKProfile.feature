#language:ru
#@test

Функционал: VK API
  - Получить всю информацию о текущем профиле
  - Заполнить недостающую информацию
  - Сменить фото профиля на любую другую

  Сценарий: получить информацию о профиле

    * создать запрос
      | method | path                    |
      | GET    | /account.getProfileInfo |

    * добавить query параметры

    * отправить запрос
    * статус код 200

    * извлечь данные
      | first_name       | $..first_name       |
      | id               | $..id               |
      | last_name        | $..last_name        |
      | home_town        | $..home_town        |
      | status           | $..status           |
      | bdate            | $..bdate            |
      | bdate_visibility | $..bdate_visibility |
      | phone            | $..phone            |
      | relation         | $..relation         |
      | sex              | $..sex              |


  # заполнить недостающую информацию
    * сравнить и заполнить если истина
      | first_name       | == |  | RRDDEE |
      | id               | == |  | EERREE |
      | last_name        | == |  | EEDDRR |
      | home_town        | == |  | EEDDDD |
      | status           | == |  | EERRRR |
      | bdate            | == |  | ERDDRE |
      | bdate_visibility | == |  | DDDDEE |
      | phone            | == |  | RREDEE |
      | relation         | == |  | EREDEE |
      | sex              | == |  | EEDDEE |

    * создать запрос
      | method | path                     |
      | GET    | /account.saveProfileInfo |

    * добавить query параметры
      | first_name       | ${first_name}       |
      | id               | ${id}               |
      | last_name        | ${last_name}        |
      | home_town        | ${home_town}        |
      | status           | ${status}           |
      | bdate            | ${bdate}            |
      | bdate_visibility | ${bdate_visibility} |
      | phone            | ${phone}            |
      | relation         | ${relation}         |
      | sex              | ${sex}              |
    * отправить запрос

# сменить фото профиля на любую другую
    * создать запрос
      | method | path                              |
      | POST   | /photos.getOwnerPhotoUploadServer |
    * добавить query параметры
      | owner_id | 87309163 |
    * отправить запрос
    * статус код 200

    * извлечь данные
      | upload_url | $..upload_url |

    * создать запрос
      | method | url           | body   |
      | POST   | ${upload_url} | 13.jpg |

    * добавить query параметры из "upload_url"

    * добавить header
      | Content-Type | multipart/form-data |

    * отправить запрос
    * статус код 200

    * извлечь данные
      | server | $..server |
      | hash   | $..hash   |
      | photo  | $..photo  |

    * создать запрос
      | method | path                   |
      | GET    | /photos.saveOwnerPhoto |

    * добавить header
      | Content-Type | application/json |

    * добавить query параметры
      | server   | ${server} |
      | hash     | ${hash}   |
      | photo    | ${photo}  |
      | owner_id | 87309163  |

    * отправить запрос

