#language:ru
#@test

Функционал: VK API
  - создать группу
  - Добавить новую тему для обсуждения
  - Закрепить тему в списке обсуждений группы
  - Написать 3 комментария с произвольным текстом
  - Поменять текст в предпоследнем комментарии
  - Удалить первый комментарий

  Сценарий: создать группу

    * создать запрос
      | method | path           |
      | GET    | /groups.create |

    * добавить query параметры
      | title | Test group VK API2 |

    * отправить запрос

# Добавить новую тему для обсуждения
    * извлечь данные
      | group_id | $..id   |
      | name     | $..name |


    * создать запрос
      | method | path            |
      | GET    | /board.addTopic |

    * добавить query параметры
      | group_id | ${group_id}                  |
      | title    | ${name}                      |
      | text     | Тестовая тема для обсуждения |

    * отправить запрос


#  Закрепить тему в списке обсуждений группы
    * извлечь данные
      | topic_id | $..response |
    * создать запрос
      | method | path            |
      | POST   | /board.fixTopic |
    * добавить query параметры
      | group_id | ${group_id} |
      | topic_id | ${topic_id} |

    * отправить запрос

#   Написать 3 комментария с произвольным текстом
    * создать запрос
      | method | path                 |
      | POST   | /board.createComment |
    * добавить query параметры
      | group_id | ${group_id}                   |
      | topic_id | ${topic_id}                   |
      | message  | Что такое multiply/form data? |

    * отправить запрос
    * статус код 200
    * извлечь данные
      | comment_id1 | $..response |
    * отправить запрос
    * статус код 200
    * извлечь данные
      | comment_id2 | $..response |
    * отправить запрос
    * статус код 200


#   Поменять текст в предпоследнем комментарии
    * создать запрос
      | method | path               |
      | GET    | /board.editComment |
    * добавить query параметры
      | group_id   | ${group_id}                                       |
      | topic_id   | ${topic_id}                                       |
      | comment_id | ${comment_id2}                                    |
      | message    | Что такое multiply/form data? И что с ним делать? |

    * отправить запрос

#  Удалить первый комментарий
    * создать запрос
      | method | path                 |
      | POST   | /board.deleteComment |
    * добавить query параметры
      | group_id   | ${group_id}    |
      | topic_id   | ${topic_id}    |
      | comment_id | ${comment_id1} |

    * отправить запрос
    * статус код 200